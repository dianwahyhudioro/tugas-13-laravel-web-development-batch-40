<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/data-table', function(){
    return view('halaman.datatable');
});

Route::get('/table', function(){
    return view('halaman.table');
});

//CRUD Cast
//Create Cast
Route::get('/cast/create', [CastController::class, 'create']);

//Save Cast
Route::post('/cast', [CastController::class, 'store']);

//View 
Route::get('/cast', [CastController::class, 'index']);

//View Detail
Route::get('/cast/{id}', [CastController::class, 'show']);

//View Update 
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);

//Update
Route::put('/cast/{id}', [CastController::class, 'update']);

//Delete
Route::delete('/cast/{id}', [CastController::class, 'destroy']);