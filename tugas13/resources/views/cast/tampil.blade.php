@extends('layout.master')

@section('title')
    Halaman Tampil Cast
@endsection

@section('sub-title')
    Cast
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah</a>
    <table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Aksi</th>
        </tr>    
    </thead>
    <tbody>
        @forelse ($cast_view as $key => $val)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$val->nama}}</td>
                <td>
                    <form action="/cast/{{$val->id}}" method="POST">
                        @method('delete')
                        @csrf
                        <a href="cast/{{$val->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="cast/{{$val->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan=3>Data Cast Kosong</td>
            </tr>
        @endforelse
    </tbody>
    </table>
@endsection
        
