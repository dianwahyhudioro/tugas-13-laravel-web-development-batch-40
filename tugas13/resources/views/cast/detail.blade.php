@extends('layout.master')

@section('title')
    Halaman Detail Cast
@endsection

@section('sub-title')
    Cast
@endsection

@section('content')
    <h1>{{$cast->nama}}</h1>
    <p>Umur : {{$cast->umur}}</p>
    <p>Biodate : {{$cast->bio}}</p>
    <a href="/cast" class="btn btn-primary btn-sm my-3 px3">Back</a>
@endsection