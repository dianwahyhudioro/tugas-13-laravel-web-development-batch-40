@extends('layout.master')

@section('title')
    Halaman Edit Cast
@endsection

@section('sub-title')
    Cast
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @method('put')
        @csrf
        <div class="form-group">
            <label for="txtnama">Nama</label>
            <input type="text" class="form-control" name="txtnama" id="txtnama" value="{{$cast->nama}}">
        </div>
        @error('txtnama')
            <div class="alert alert-danger">{{$message}} </div>
        @enderror
        <div class="form-group">
            <label for="txtumur">Umur</label>
            <input type="number" name="txtumur" id="txtumur" class="form-control" value="{{$cast->umur}}">
        </div>
        @error('txtumur')
            <div class="alert alert-danger">{{$message}} </div>
        @enderror
        <div class="form-group">
            <label for="txtbio">Biodata</label>
            <textarea name="txtbio" id="txtbio" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
        </div>
        @error('txtbio')
            <div class="alert alert-danger">{{$message}} </div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="/cast" class="btn btn-primary btn-sm my-3 px3">Back</a>
  </form>    
@endsection
        
