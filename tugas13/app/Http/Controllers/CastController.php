<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller{
    public function create(){
        return view('cast.tambah');
    }

    public function store(Request $request){
        $request->validate([
            'txtnama' => 'required',
            'txtumur' => 'required',
            'txtbio' => 'required'
        ],
        [
            'txtnama.required' => 'Nama harus terisi',
            'txtumur.required' => 'Umur harus terisi',
            'txtbio.required' => 'Biodata harus terisi'
        ]);

        DB::table('cast')->insert([
            'nama' => $request['txtnama'],
            'umur' => $request['txtumur'],
            'bio' => $request['txtbio'],
            'created_at' => date('Y-m-d H:i:s')
        ]);

        return redirect('/cast');
    }

    public function index(){
        $cast_view = DB::table('cast')->get();
        // dd($cast_view);

        return view('cast.tampil', ['cast_view' => $cast_view]);
    }

    public function show($id){
        $cast = DB::table('cast')->find($id);

        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id){
        $cast = DB::table('cast')->find($id);

        return view('cast.edit', ['cast' => $cast]);
    }

    public function update($id, Request $request){
        $request->validate([
            'txtnama' => 'required',
            'txtumur' => 'required',
            'txtbio' => 'required'
        ],
        [
            'txtnama.required' => 'Nama harus terisi',
            'txtumur.required' => 'Umur harus terisi',
            'txtbio.required' => 'Biodata harus terisi'
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['txtnama'],
                'umur' => $request['txtumur'],
                'bio' => $request['txtbio'],
                'updated_at' => date('Y-m-d H:i:s')    
            ]);

        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
